import io
import json
import subprocess
from datetime import timedelta
from unittest.mock import patch, Mock, MagicMock, ANY

import numpy
import pytest
from itertools import islice
from time import sleep

from stream_video.decode import VideoStreamDecoder, FFmpegNotFound, VideoProbeFailed, \
    VideoReadTimeout, VideoSubprocessError, VideoDecodeError


@pytest.fixture(name='probe_data')
def _probe_data():
    return {
        "programs": [],
        "streams": [
            {"avg_frame_rate": "0/0"},
            {
                "width": 720,
                "height": 500,
                "pix_fmt": "yuvj420",
                "color_space": "bt406",
                "r_frame_rate": "4/1",
                "avg_frame_rate": "4/1"
            },
            {"avg_frame_rate": "0/0"}
        ]
    }


@pytest.fixture(name='probe_stdout')
def _probe_stdout(probe_data):
    return json.dumps(probe_data)


@pytest.fixture(name='frame')
def _frame():
    return numpy.random.randint(0, 256, 500 * 720 * 3, dtype='uint8').reshape((500, 720, 3))


@pytest.fixture(name='frame_buffer')
def _frame_buffer(frame):
    # Create a buffer with enough data for 2 frames, but corrupt the second frame.
    # This ensures that the correct amount of data is read for a single frame.
    return io.BytesIO(frame.tobytes() + b'abc123' + frame.tobytes())


@pytest.fixture(name='mock_select')
def _mock_select(frame_buffer):
    mock = Mock(name='select', return_value=([frame_buffer], [], []))
    with patch(VideoStreamDecoder.__module__ + '.select', new=mock):
        yield mock


@patch('subprocess.Popen', **{
    'return_value.poll.return_value': None
})
@patch('subprocess.run', **{'return_value.stdout': json.dumps({
    "streams": [{
        "width": 720,
        "height": 500,
        "r_frame_rate": "25/1"
    }]
})})
class TestDecode:

    def test_init_invalid_protocol(self, mock_run, mock_popen):
        pass

    def test_probe_success(self, mock_run, mock_popen, probe_stdout):
        mock_run.return_value.stdout = probe_stdout
        stream = VideoStreamDecoder('url')
        assert stream.width == 720 and stream.height == 500 and stream.fps_expected == 4

    def test_probe_command_not_found(self, mock_run, mock_popen):
        mock_run.side_effect = FileNotFoundError
        with pytest.raises(FFmpegNotFound):
            VideoStreamDecoder('url')
        mock_popen.assert_not_called()

    def test_probe_invalid_data(self, mock_run, mock_popen):
        mock_run.return_value.stdout = b''
        with pytest.raises(VideoProbeFailed, match=r'Unparsable result'):
            VideoStreamDecoder('url')
        mock_popen.assert_not_called()

    def test_probe_timeout(self, mock_run, mock_popen):
        mock_run.side_effect = subprocess.TimeoutExpired([], 1)
        with pytest.raises(VideoProbeFailed, match=r'timeout'):
            VideoStreamDecoder('url')
        mock_popen.assert_not_called()

    def test_probe_return_code_error(self, mock_run, mock_popen):
        mock_run.side_effect = subprocess.CalledProcessError(1, [])
        with pytest.raises(VideoProbeFailed, match=r'exit code 1'):
            VideoStreamDecoder('url')
        mock_popen.assert_not_called()

    def test_probe_no_streams(self, mock_run, mock_popen, probe_data):
        probe_data['streams'] = []
        mock_run.return_value.stdout = json.dumps(probe_data)
        with pytest.raises(VideoProbeFailed, match=r'Unparsable'):
            VideoStreamDecoder('url')

    def test_probe_no_width(self, mock_run, mock_popen, probe_data):
        del probe_data['streams'][1]['width']
        mock_run.return_value.stdout = json.dumps(probe_data)
        with pytest.raises(VideoProbeFailed, match=r'Unparsable'):
            VideoStreamDecoder('url')

    def test_probe_no_height(self, mock_run, mock_popen, probe_data):
        del probe_data['streams'][1]['height']
        mock_run.return_value.stdout = json.dumps(probe_data)
        with pytest.raises(VideoProbeFailed, match=r'Unparsable'):
            VideoStreamDecoder('url')

    def test_probe_bad_fps(self, mock_run, mock_popen, probe_data):
        probe_data['streams'][1]['r_frame_rate'] = 'a/b'
        mock_run.return_value.stdout = json.dumps(probe_data)
        with pytest.raises(VideoProbeFailed, match=r'Unparsable'):
            VideoStreamDecoder('url')

    def test_probe_zero_fps(self, mock_run, mock_popen, probe_data):
        probe_data['streams'][1]['avg_frame_rate'] = '0/0'
        probe_data['streams'][1]['r_frame_rate'] = '0/0'
        mock_run.return_value.stdout = json.dumps(probe_data)
        stream = VideoStreamDecoder('url')
        assert stream.width == 720 and stream.height == 500
        assert stream.fps_expected is None

    def test_probe_no_fps_or_colour_or_avg(self, mock_run, mock_popen, probe_data):
        del probe_data['streams'][1]['avg_frame_rate']
        del probe_data['streams'][1]['pix_fmt']
        del probe_data['streams'][1]['color_space']
        del probe_data['streams'][1]['r_frame_rate']
        mock_run.return_value.stdout = json.dumps(probe_data)
        stream = VideoStreamDecoder('url')
        assert stream.width == 720 and stream.height == 500
        assert stream.fps_expected is None

    def test_probe_zero_fps_correct_avg(self, mock_run, mock_popen, probe_data):
        probe_data['streams'][1]['avg_frame_rate'] = '0/0'
        mock_run.return_value.stdout = json.dumps(probe_data)
        stream = VideoStreamDecoder('url')
        assert stream.width == 720 and stream.height == 500
        assert stream.fps_expected == 4.0

    def test_probe_no_fps_correct_avg(self, mock_run, mock_popen, probe_data):
        del probe_data['streams'][1]['avg_frame_rate']
        del probe_data['streams'][1]['pix_fmt']
        del probe_data['streams'][1]['color_space']
        mock_run.return_value.stdout = json.dumps(probe_data)
        stream = VideoStreamDecoder('url')
        assert stream.width == 720 and stream.height == 500
        assert stream.fps_expected == 4.0

    def test_connect_success(self, mock_run, mock_popen):
        VideoStreamDecoder('rtsp://camera1')
        mock_popen.assert_called_once()

    def test_connect_command_not_found(self, mock_run, mock_popen):
        mock_popen.side_effect = FileNotFoundError
        with pytest.raises(FFmpegNotFound):
            VideoStreamDecoder('url')

    def test_read_frame(self, mock_run, mock_popen, mock_select, frame):
        stream = VideoStreamDecoder('url')
        assert numpy.array_equal(frame, stream.read_frame(1))

    def test_read_frame_chunked(self, mock_run, mock_popen, mock_select, frame):
        """
        read called multiple times until full frame is read
        """
        frame_bytes = frame.tobytes()
        mock_select.side_effect = [
            ([io.BytesIO(frame_bytes[:1024])], [], []),
            ([io.BytesIO(frame_bytes[1024:])], [], [])
        ]
        stream = VideoStreamDecoder('url')
        assert numpy.array_equal(frame, stream.read_frame(1))

    def test_read_frame_subprocess_early_exit(self, mock_run, mock_popen, mock_select, frame):
        frame_bytes = frame.tobytes()
        mock_select.side_effect = [
            ([io.BytesIO(frame_bytes[:1024])], [], []),
            ([io.BytesIO(frame_bytes[1024:])], [], [])
        ]
        mock_popen.return_value.poll.side_effect = [None, 1]
        stream = VideoStreamDecoder('url')
        with pytest.raises(VideoSubprocessError, match=r'early exit'):
            stream.read_frame(1)

    @patch('time.time')
    def test_read_frame_timeout(self, mock_time, mock_run, mock_popen, mock_select, frame):
        frame_bytes = frame.tobytes()
        mock_select.side_effect = [
            ([io.BytesIO(frame_bytes[:1024])], [], []),
            ([io.BytesIO(frame_bytes[1024:2048])], [], []),
            ([], [], [])
        ]
        mock_time.side_effect = [10.0, 15.0, 20.0, 25.0]
        with pytest.raises(VideoReadTimeout, match=r'read 2048 of 1080000 bytes'):
            VideoStreamDecoder('url').read_frame(10)

    @patch('time.time')
    def test_read_frame_no_timeout_when_full_frame_available(
            self, mock_time, mock_run, mock_popen, mock_select, frame):
        """
        if elapsed > timeout but the full frame could be read, don't raise
        """
        frame_bytes = frame.tobytes()
        mock_select.side_effect = [
            ([io.BytesIO(frame_bytes[:1024])], [], []),
            ([io.BytesIO(frame_bytes[1024:])], [], []),
            ([], [], [])
        ]
        mock_time.side_effect = [10.0, 15.0, 20.0, 25.0]
        stream = VideoStreamDecoder('url')
        assert numpy.array_equal(frame, stream.read_frame(10))

    def test_read_frame_numpy_error(self, mock_run, mock_popen, mock_select, frame):
        """
        if pipe.read(size) returns more than `size` bytes, reshape will fail
        """
        frame_bytes = frame.tobytes()
        mock_select.side_effect = [
            ([io.BytesIO(frame_bytes[:1024])], [], []),
            ([Mock(read=lambda _: frame_bytes[1024:] + frame_bytes)], [], [])
        ]
        stream = VideoStreamDecoder('url')
        with pytest.raises(VideoDecodeError):
            stream.read_frame(1)

    @patch('time.time')
    def test_read_frame_after_timeout(self, mock_time, mock_run, mock_popen, mock_select, frame):
        """
        Reading a frame with timeout may cause part of a frame to be read only.
        Confirm that reading the next frame will succeed by including any data read from before
        the timeout of the first read.
        """
        frame_bytes = frame.tobytes()
        mock_select.side_effect = [
            ([io.BytesIO(frame_bytes[:1024])], [], []),
            ([io.BytesIO(frame_bytes[1024:2048])], [], []),
            ([io.BytesIO(frame_bytes[2048:])], [], [])
        ]
        mock_time.side_effect = range(10)
        stream = VideoStreamDecoder('url')
        with pytest.raises(VideoReadTimeout, match=r'read 2048 of 1080000 bytes'):
            stream.read_frame(2)
        assert numpy.array_equal(frame, stream.read_frame(1))

    def test_iteration(self, mock_run, mock_popen, mock_select, frame):
        mock_select.side_effect = lambda *_: ([io.BytesIO(frame.tobytes())], [], [])
        stream = VideoStreamDecoder('url')
        items = 0
        for f in islice(stream, 3):
            assert numpy.array_equal(frame, f)
            items += 1
        assert items == 3

    @patch('time.time')
    def test_iteration_timeout(self, mock_time, mock_run, mock_popen, mock_select, frame):
        frame_bytes = frame.tobytes()
        mock_select.side_effect = [
            ([io.BytesIO(frame_bytes)], [], []),
            # frame #2, in 2 parts
            ([io.BytesIO(frame_bytes[:1024])], [], []),
            ([io.BytesIO(frame_bytes[1024:])], [], []),
            # frame #3, in 3 parts
            ([io.BytesIO(frame_bytes[:1024])], [], []),
            ([io.BytesIO(frame_bytes[1024:2048])], [], []),
            ([io.BytesIO(frame_bytes[2048:])], [], [])
        ]
        mock_time.side_effect = range(10, 20)
        stream = VideoStreamDecoder('url', frame_timeout=2)
        items = 0
        with pytest.raises(VideoReadTimeout):
            for f in stream:
                assert numpy.array_equal(frame, f)
                items += 1
        assert items == 2

    def test_context_manager_usage(self, mock_run, mock_popen):
        with VideoStreamDecoder('url') as stream:
            pass
        mock_popen.return_value.terminate.assert_called_once()

    def test_close(self, mock_run, mock_popen):
        stream = VideoStreamDecoder('url')
        mock_popen.return_value.terminate.assert_not_called()
        stream.close()
        mock_popen.return_value.terminate.assert_called_once()

    @patch('time.time')
    def test_fps_observed(self, mock_time, mock_run, mock_popen, mock_select, frame):
        mock_select.side_effect = lambda *_: ([io.BytesIO(frame.tobytes())], [], [])
        # time is read 3 times per frame (assuming the entire frame is read from buffer in one go):
        #  1) start of frame read, to track timeout
        #  2) end of frame read, to record FPS
        #  3) when reading from the fps_observed property
        times_per_frame = 3
        mock_fps = 10
        iterations = mock_fps * 3
        # progress time by 1/fps every frame cycle
        mock_time.side_effect = [
            1 + i//times_per_frame/mock_fps
            for i in range(iterations * times_per_frame)
        ]

        stream = VideoStreamDecoder('url')

        # before any frames received, FPS is zero
        assert stream.fps_observed == 0.0

        # start streaming
        for i, _ in enumerate(islice(stream, iterations)):
            if i == 0:
                # when only 1 frame received, FPS cannot be calculated
                assert stream.fps_observed == 0.0
            elif i < mock_fps:
                # during the first second, FPS is < observed
                assert 0 < stream.fps_observed < mock_fps
            else:
                assert stream.fps_observed == pytest.approx(mock_fps)

    @patch('time.time')
    def test_fps_observed_large_gap(self, mock_time, mock_run, mock_popen, mock_select, frame):
        """
        after a large gap, FPS should jump back to zero
        """
        mock_select.side_effect = lambda *_: ([io.BytesIO(frame.tobytes())], [], [])
        mock_time.side_effect = range(10)
        stream = VideoStreamDecoder('url')
        for _ in islice(stream, 3):
            pass
        mock_time.side_effect = [15]
        assert stream.fps_observed == 0

    @patch('time.time')
    @patch('time.sleep')
    def test_monitor_fps(self, mock_sleep, mock_time, mock_run, mock_popen, mock_select, frame):
        mock_select.side_effect = lambda *_: ([io.BytesIO(frame.tobytes())], [], [])
        mock_time.side_effect = range(100)
        # only allow the monitor thread to sleep briefly
        mock_sleep.side_effect = lambda t: sleep(0.1)
        monitor = MagicMock(name='Monitor')

        stream = VideoStreamDecoder(
            'rtsp://user:pass@host/path', monitor=monitor)
        with stream:
            for _ in islice(stream, 5):
                pass
            sleep(0.15)  # allow time for metric to be recorded before closing the stream
        monitor.put_metric.assert_called_once_with(
            'FPS', 0.5, dimensions={'source': 'rtsp://host/path'}, unit=ANY)

        # Ensure no more metrics produced, now that stream has been stopped
        sleep(0.2)
        assert monitor.put_metric.call_count == 1
