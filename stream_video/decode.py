import json

from rb_service_monitor.connector import MonitorPutTimeout, MetricUnit, ServiceMonitorConnector
from collections import deque
from datetime import timedelta
from urllib.parse import urlparse, urlunparse
from io import BytesIO
from select import select
import subprocess
import numpy as np
import threading
import logging
import time


NETWORK_PROTOCOLS = {'tcp', 'udp', 'udp_multicast', 'http'}
TRACE = 5
HIGH_FPS = 25  # assume that typically we won't be streaming at > 25fps
DEFAULT_METRIC_INTERVAL = timedelta(seconds=5)
FPS_OVERFLOW = 90000  # the value at which we assume FFmpeg has failed to determine FPS


class VideoStreamDecoder(object):
    """
    The video stream decoder connects to a video stream (URL-based) and generates
    video frames as numpy arrays. The video frames are of the shape (height, width, 3).

    The implementation uses FFmpeg under the hood, and will spawn an ffmpeg subprocess
    to connect to a video stream and perform the decoding. Note that FFmpeg is not 100%
    reliable and can get stuck in a zombie state where no data is returned but the process
    is still alive; these situations are handled by raising a timeout error upon frame read.
    """

    def __init__(
            self, url: str,
            monitor: ServiceMonitorConnector = None, metric_interval: timedelta = None,
            network_protocol: str = 'tcp',
            probe_timeout: int = 10, frame_timeout: int = 10,
            throttle_fps: float = None, force_throttle: bool = False):
        """
        Probe and connect to the video stream, and start monitoring thread.

        :param url: The url of the camera stream which will be pulled
        :param monitor: The service monitor used to report fps metric
        :param metric_interval: The interval at which the monitor reports metrics
        :param network_protocol: The network protocol used to connect to the stream
        :param probe_timeout: The time allowed for ffprobe to fetch the meta data of the stream
        :param frame_timeout: The time allowed for the application to read a frame before raising
        :param throttle_fps: Set the throttling frame rate.
        :param force_throttle: Always throttle, even when the source reports FPS.
        """

        # Set up logging
        self._log = logging.getLogger(__name__)

        # Set up sub process variables
        self._url = url
        self._redacted_url = url_redact(self._url)
        self._frame_timeout = frame_timeout
        self._video_process = None
        self._frame_buffer = None
        if network_protocol not in {None} | NETWORK_PROTOCOLS:
            raise ValueError(
                "Network protocol must be one of {}, but got '{}'".format(
                    '/'.join(NETWORK_PROTOCOLS), network_protocol))

        self._ff_input_args = []
        if urlparse(url)[0] == 'rtsp':
            self._ff_input_args += ['-rtsp_transport', network_protocol or 'tcp']
        # Peripheral camera support (mainly for testing)...
        # https://trac.ffmpeg.org/wiki/Capture/Webcam
        elif url.isnumeric() or url in ('default', 'Integrated'):  # OSX
            self._ff_input_args += \
                ['-f', 'avfoundation', '-framerate', str(throttle_fps or 30)]
        elif url.startswith('/dev/'):  # Linux
            self._ff_input_args += \
                ['-f', 'v4l2', '-framerate', str(throttle_fps or 25), '-video_size', '640x480']

        # FPS tracking
        metric_interval = (metric_interval or DEFAULT_METRIC_INTERVAL).total_seconds()
        # timestamps of the most recent frames
        self._throttle = force_throttle
        self._throttle_fps = throttle_fps
        self._frame_times = deque(maxlen=int(metric_interval * HIGH_FPS))
        self._fps_window = metric_interval  # how many seconds to average FPS over
        self._fps_observed = 0.0  # calculated FPS
        self._fps_updated = None  # timestamp when FPS was updated

        # Metadata
        self._width = None
        self._height = None
        self._fps_expected = None
        self._tbr_expected = None

        # Probe the stream for the stream details
        self._probe(probe_timeout)

        # Start the sub process to fetch and convert the stream to raw video
        self._connect()

        # Start the monitoring thread
        self._running = True
        if monitor or self._log.isEnabledFor(logging.DEBUG):
            thread = threading.Thread(
                target=self._monitor_thread, args=(monitor, metric_interval))
            thread.setDaemon(True)
            thread.start()

    def __iter__(self):
        """
        Frame Generator pulls the camera stream and converts the stream to a numpy array
        :return: Iterator of numpy array representation of each frame
        """
        while True:
            yield self.read_frame()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        self._running = False  # stop monitor
        if self._video_process is None:
            return
        self._video_process.terminate()
        try:
            self._video_process.wait(1)
        except subprocess.TimeoutExpired:
            self._video_process.kill()
            self._video_process.wait()

    @property
    def fps_observed(self):
        """
        Return the current average frame rate (frames received per second).
        0 will be returned if no frames have been received within the FPS time-window.
        This property is thread safe, and can be read from background threads.
        :return: Observed frames per second metric.
        """
        if self._fps_updated and time.time() - self._fps_updated < self._fps_window:
            return self._fps_observed
        return 0.0

    @property
    def fps_expected(self):
        """
        :return: Observed frames per second metric, indicating the average
        number of frames received per second
        """
        return self._fps_expected

    @property
    def width(self):
        """
        :return: The width of the stream fetched from the camera
        """
        return self._width

    @property
    def height(self):
        """
        :return: The width of the stream fetched from the camera
        """
        return self._height

    def read_frame(self, timeout=None):
        """
        Read the next frame from the video as a numpy array with dimensions (width, height, 3).
        :param timeout:
            Limit the read operation to this many seconds.
            Defaults to the value provided in the constructor.
        :return: Image representing the next video frame as a numpy array.
        """
        timeout = timeout or self._frame_timeout

        # Throttle (enabled when throttling is forced, or when FPS could not be probed)
        if (self._throttle or not self._fps_expected) and self._throttle_fps and self._frame_times:
            # determine the time for the next frame to be grabbed according to the full fps buffer
            # so as to smooth the playback and allow it to recover from fluctuations
            # (rather than simply using the time of the frame most recently received).
            next_frame_time = \
                1 / self._throttle_fps * (len(self._frame_times) + 1) + self._frame_times[0]
            time.sleep(max(0, next_frame_time - time.time()))

        # Read, with timeout
        length = self._width * self._height * 3
        if self._frame_buffer is None:
            self._frame_buffer = BytesIO()
        total_read = len(self._frame_buffer.getvalue())
        start = time.time()

        while True:
            # Check if ffmpeg has exited or crashed
            if self._video_process.poll() is not None:
                raise VideoSubprocessError(
                    "FFmpeg subprocess early exit with code: {}".format(
                        self._video_process.returncode))
            # Reading raw frames from the output of the pipe
            # the arithmetic represents how much to read from the stdout
            readable, _, _ = select([self._video_process.stdout], [], [], 1)
            for s in readable:
                bytes_next = s.read(length - total_read)
                total_read += len(bytes_next)
                self._frame_buffer.write(bytes_next)
            # Check completion (before timeout)
            if total_read >= length:
                break
            # Check timeout
            elapsed = time.time() - start
            self._log.log(TRACE, "elapsed time taken to attempt reading frame {}".format(elapsed))
            if 0 < timeout <= elapsed:
                raise VideoReadTimeout(
                    'Frame read timeout of {}s reached, read {} of {} bytes'.format(
                        timeout, total_read, length))

        # Parse; convert the raw images to numpy arrays and reshape the array.
        try:
            shape = (self._height, self._width, 3)
            image = np.frombuffer(self._frame_buffer.getbuffer(), dtype='uint8').reshape(shape)
        except ValueError as e:
            raise VideoDecodeError(
                'Numpy could not read the raw image from buffer: {!r}'.format(e))
        finally:
            # now that the frame has been read in full, clear the buffer
            self._frame_buffer = None

        # Update FPS
        self._fps_put_frame()

        return image

    def _probe(self, timeout=0):
        """
        ffprobe connects to the stream and fetches the metadata of the stream.
        """
        self._log.info("Probing stream {}...".format(self._redacted_url))

        # ffmpeg command to retrieve the stream metadata
        probe_cmd = [
            'ffprobe',
            # default probe log level to match Pythons, as probe is only performed once at startup
            '-v', str(ffmpeg_log_level(self._log.getEffectiveLevel(), adjust=-1)),
            '-of', 'json',
            '-show_entries', 'stream=width,height,avg_frame_rate,r_frame_rate,color_space,pix_fmt'
        ] + self._ff_input_args + [self._url]

        # Run
        try:
            probe_output = subprocess.run(
                probe_cmd, timeout=timeout, stdout=subprocess.PIPE, check=True).stdout
        except FileNotFoundError as e:
            raise FFmpegNotFound("'ffprobe' not found by python: {!r}".format(e))
        except subprocess.TimeoutExpired as e:
            raise VideoProbeFailed(
                "FFprobe could not connect to stream within the {}s timeout "
                "and retrieve the meta data".format(timeout)
            )
        except subprocess.CalledProcessError as e:
            raise VideoProbeFailed(
                "FFprobe failed with exit code {}".format(e.returncode))

        # Parse
        try:
            streams = json.loads(probe_output)['streams']
            metadata = [stream for stream in streams if 'width' in stream][0]
            self._width = metadata['width']
            self._height = metadata['height']
            fps = 0

            # FPS from TBR
            frame, base = metadata.get('r_frame_rate', '0/0').split('/')
            frame, base = float(frame), float(base)
            if base and frame:
                fps = frame / base
                if fps and fps >= FPS_OVERFLOW:
                    self._log.warning(
                        'Probed frame rate: TBR (video container frame rate) overflow; '
                        'ignoring reported {} TBR'.format(fps))
                    fps = 0

            # Otherwise, FPS from average frame rate
            if not fps:
                fps = float(metadata.get('avg_frame_rate', '0/0').split('/')[0])
                if fps and fps >= FPS_OVERFLOW:
                    self._log.warning(
                        'Probed frame rate: average frame rate overflow; '
                        'ignoring reported {} fps'.format(fps))
                    fps = 0
                if fps:
                    self._log.info(
                        'Probed frame rate: TBR (video container frame rate) could not be probed; '
                        'falling back to average frame rate')

            # Otherwise, FPS from configured throttle speed
            if not fps:
                if self._throttle_fps:
                    self._log.warning(
                        'Probe failed to determine video frame rate; defaulting to provided {}fps '
                        'throttle rate.'.format(self._throttle_fps))
                    fps = self._throttle_fps
                else:
                    self._log.warning(
                        'Probe failed to determine video frame rate, and no throttle frame rate '
                        'was provided. Beware that FFmpeg can produce too many frames from streams '
                        'which do not report their frame rate. Consider specifying a throttle '
                        'frame rate.')

            # store fps, and default the throttle
            self._fps_expected = fps or None
            if fps and not self._throttle_fps:
                self._throttle_fps = fps

        except (ValueError, IndexError, KeyError) as e:
            raise VideoProbeFailed(
                "Unparsable result from FFprobe: {!r}\n{}".format(e, probe_output))

        self._log.info(
            'Probed video stream: {}x{} @ {} fps, {} tbr, {} pixel format, {} colour space'.format(
                self._width, self._height,
                metadata.get('avg_frame_rate', '?'), metadata.get('r_frame_rate', '?'),
                metadata.get('pix_fmt', '?'), metadata.get('color_space', '?')))

    def _connect(self):
        """
        Connect to the stream using ffmpeg in a subprocess and start decoding.
        """
        self._log.info("Connecting to stream {}...".format(self._redacted_url))

        # ffmpeg command to pull a stream convert to raw video then pipe out
        raw_video_cmd = [
            'ffmpeg',
            # default ffmpeg log level to error (assuming Python @ info level), otherwise we may
            # get inundated with warnings throughout the lifetime of streaming
            '-loglevel', str(ffmpeg_log_level(self._log.getEffectiveLevel(), adjust=-2))
        ] + self._ff_input_args + [
            '-i', self._url,
            '-f', 'image2pipe',
            '-pix_fmt', 'bgr24',
            '-c:v', 'rawvideo',
            'pipe:'
        ]

        # Pull the stream converting it to raw image format
        try:
            self._video_process = subprocess.Popen(raw_video_cmd, stdout=subprocess.PIPE)
        except FileNotFoundError as e:
            raise FFmpegNotFound("'ffmpeg' not found by python: {!r}".format(e))

        self._log.info("Connected to stream: {}".format(self._redacted_url))

    def _fps_put_frame(self):
        """
        Record the generation of a new frame, and update the FPS property.
        """
        now = time.time()

        # record frame timestamp
        self._frame_times.append(now)

        # ignore frames older than fps_window
        while self._frame_times and self._frame_times[0] < now - self._fps_window:
            self._frame_times.popleft()

        # recalculate FPS, but only if at least 2 frames received
        if len(self._frame_times) >= 2:
            # FPS = number of frames / timespan (ignoring last frame in timespan)
            # However, don't allow considered timespan to be less than 1 second, in order to prevent
            # high FPS values upon initial connect (if frames received in quick succession off
            # buffer).
            self._fps_observed = \
                (len(self._frame_times) - 1) \
                / max(1, self._frame_times[-1] - self._frame_times[0])
        else:
            self._fps_observed = 0.0
        self._fps_updated = now

        # Log
        if self._log.isEnabledFor(TRACE):
            self._log.log(TRACE, "FPS observed: {}".format(self._fps_observed))

    def _monitor_thread(self, monitor, interval):
        """
        Puts FPS metrics on the service monitor
        :param monitor: ServiceMonitorConnector
        :param interval: time (seconds) between each metric output
        """
        time.sleep(interval)
        while self._running:
            fps = self.fps_observed
            if monitor:
                try:
                    monitor.put_metric(
                        "FPS", fps,
                        dimensions={'source': self._redacted_url},
                        unit=MetricUnit.count_per_second
                    )
                except MonitorPutTimeout as e:
                    self._log.warning("{!r}".format(e))
            self._log.debug('Observed average FPS is currently: {}'.format(fps))
            time.sleep(interval)


def ffmpeg_log_level(python_level, adjust=0):
    """
    Convert a Python log level into the equivalent FFmpeg log level, aligning 'info' (20) -->
    'info' (32) by default, but this is adjustable using the `adjust` parameter.
    :param python_level: The python log level.
    :param adjust: Increase (positive) or decrease (negative) the FFmpeg logging verbosity.
    :return: The equivalent FFmpeg log level
    """
    # ffmpeg debug (48) <-> python notset (0)
    # ffmpeg fatal (8) <-> python critical (50)
    # ffmpeg steps log level by 8, Python by 10
    return int(48 - ((python_level / 10 - adjust) * 8))


def url_redact(url):
    """
    Strip the credentials from a URL, so the URL is safe for logging.
    :param url: The URL to redact.
    :return: The redacted URL.
    """
    parsed_url = urlparse(url)
    return urlunparse([
        parsed_url[0],
        '{}{}'.format(
            parsed_url.hostname or '',
            ':{}'.format(parsed_url.port) if parsed_url.port else ''
        ),
        *parsed_url[2:]
    ])


class FFmpegNotFound(Exception):
    """
    One of 'ffmpeg' or 'ffprobe' commands could not be found by Python.
    """


class VideoError(Exception):
    """
    A base class for all errors to do with reading video.
    """


class VideoDecodeError(VideoError):
    """
    Failure to read a frame of video, due to inability to interpret/understand the frame data.
    """


class VideoProbeFailed(VideoError):
    """
    Failed to connect to and determine the metadata of the stream.
    """


class VideoSubprocessError(VideoError):
    """
    Unexpected early exit of the FFmpeg subprocess.
    """


class VideoReadTimeout(VideoError):
    """
    Timeout reached when attempting to read a frame of video.
    """
