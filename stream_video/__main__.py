from stream_video.decode import VideoStreamDecoder, FFmpegNotFound, VideoError, NETWORK_PROTOCOLS
from rb_service_monitor.connector import ServiceMonitorConnector, monitor_options_arg_parser
from rb_stream.producer import StreamServer, DEFAULT_PORT
import sys
import logging
import argparse


DEFAULT_STREAM_NAME = 'live/video'


def main(args=None):

    # Parse Arguments
    parser = argparse.ArgumentParser(
        description="Video Streamer",
        parents=[monitor_options_arg_parser(
            group_desc="Optionally log stream metrics by specifying a namespace.",
            include_interval=True,
            include_legacy_aliases=True,
            include_nq=True
        )]
    )
    parser.add_argument(
        "video_url",
        help="A URL string to access the video stream to pull, e.g. rtsp://192.168.0.123/stream.sdp"
    )
    parser.add_argument(
        "--stream-name", default=DEFAULT_STREAM_NAME,
        help=
        "The name to assign to rbs:// video stream being produced; used by subscribers. "
        "Defaults to '{}'.".format(DEFAULT_STREAM_NAME)
    )
    parser.add_argument(
        "--network-protocol", default="tcp", type=lambda s: s.lower(), choices=NETWORK_PROTOCOLS,
        help="Specify the network protocol; 'tcp' is default"
    )
    parser.add_argument(
        "--continuity", default="continuous",
        help=
        "Configure whether the stream is 'continuous' (default) or 'discrete'. "
        "The latter instructs consumers they should always read every frame (consumers can "
        "override), whereas 'continuous' streams allow frame sampling."
    )
    parser.add_argument(
        "--max-age", default=None, type=int,
        help="The maximum age (seconds) to buffer old frames before they are discarded"
    )
    parser.add_argument(
        "--host", default="0.0.0.0",
        help="The address that the queue will accept requests from"
    )
    parser.add_argument(
        "--port", default=DEFAULT_PORT, type=int,
        help="The port that the queue will accept requests from"
    )
    parser.add_argument(
        "--hwm", default=100, type=int,
        help=
        "The High Water Mark (HWM) which defines how many video frames we are willing to buffer "
        "on the outbound queue, per consumer, before dropping messages. Dropped messages will "
        "cause consumers to abort, which is desirable as consumers are generally responsible for "
        "their own buffering."
    )
    parser.add_argument(
        "--probe-timeout", default=60, type=int,
        help=
        "The maximum amount of time (in seconds) to attempt to connect to the stream initially. "
        "If reached, the connection is aborted. Defaults to 60 seconds."
    )
    parser.add_argument(
        "--frame-timeout", default=10, type=int,
        help=
        "The timeout (in seconds) in which the stream is allowed to not receive any frames before "
        "aborting. Defaults to 10 seconds."
    )
    parser.add_argument(
        "--throttle", action="store_true", help=
        "Refuse to read from the video source at a frame rate faster than the --throttle-fps frame "
        "rate, or the reported frame rate of the camera if unset. This can be useful for reading "
        "from files, or reading from video sources where the source FPS is suspected of being "
        "reported incorrectly (which can cause duplicate frames to be received at a high rate). "
        "However, typically it should be enough to set --throttle-fps only, which enables "
        "throttling only when the source FPS cannot be probed.")
    parser.add_argument(
        "--throttle-fps", type=float, help=
        "Configure a maximum frame rate to read from the video source. This only applies if either "
        "the video source does not report its frame rate (because the source is suspected of "
        "sending too many frames) or if the --throttle flag is set (which forces throttling)."
    )
    parser.add_argument(
        "-v", "--verbose", default=0, action="count",
        help='Increase the amount of logging output. Can be specified multiple times.'
    )
    parser.add_argument(
        "-q", "--quiet", default=0, action="count",
        help='Decrease the amount of logging output. Can be specified multiple times.'
    )

    args = parser.parse_args(args)
    monitor_connector = ServiceMonitorConnector.from_args(args)
    logger_level = max(1, logging.INFO + (args.quiet - args.verbose) * 10)

    # Configure logging
    logging.basicConfig(level=logger_level)
    log = logging.getLogger(__name__)

    try:
        with VideoStreamDecoder(
                args.video_url, monitor=monitor_connector,
                metric_interval=args.metric_interval,
                network_protocol=args.network_protocol,
                probe_timeout=args.probe_timeout,
                frame_timeout=args.frame_timeout,
                throttle_fps=args.throttle_fps,
                force_throttle=args.throttle
        ) as stream:
            meta = {
                'fps': stream.fps_expected, 'height': stream.height, 'width': stream.width,
                'is_color': True
            }
            stream_server = StreamServer(
                host=args.host,
                port=args.port,
                high_water_mark=args.hwm
            )
            stream_server.add_stream(
                args.stream_name,
                lambda: ((None, f) for f in stream),
                meta=meta,
                encoding="numpy",
                continuity=args.continuity,
                max_age=args.max_age
            )
            log.info("Stream server has begun streaming")
            stream_server.stream_forever()
    except (FFmpegNotFound, VideoError) as e:
        return '{}: {}'.format(type(e).__name__, e)
    except KeyboardInterrupt:
        log.info('Interrupted by user')


if __name__ == '__main__':
    sys.exit(main())
