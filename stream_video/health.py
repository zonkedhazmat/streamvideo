from rb_stream.consumer import Stream
import sys


def health_check():
    with Stream('') as stream:
        for value in stream:
            if not len(value[1]):
                return 'Stream contains no data'
            break


if __name__ == '__main__':
    sys.exit(health_check())
