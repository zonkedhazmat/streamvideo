# Video Streamer #

_Video Streamer_ pulls a video stream and decodes each frame to a _numpy_ array, and re-streams the
decoded video as a _RocketBoots Stream_, allowing services to subscribe the the stream.

## Usage

Stream Video is a commandline application, the following is an example of its usage. 

```
stream_video 'rtsp://USER:PASS@IPADDRESS/camera-specific-args'
```

Use the help output for more information:

```
stream_video --help
```

There is also a video player application to visualise the stream when running locally for testing
purposes:

```
python doc/connect.py
```

## Docker

Includes a Docker file to build an image. The image can be used in production where we can expect
each Docker host to have one container running per camera stream. The image can also be used during
development and testing. 
